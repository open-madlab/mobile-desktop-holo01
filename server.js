// Based off of Shawn Van Every's Live Web
// http://itp.nyu.edu/~sve204/liveweb_fall2013/week3.html

// Using express: http://expressjs.com/
var express = require('express');
//var contentfullVar = require('contentful');////Server error: error contentful@10.11.0: The engine "node" is incompatible with this module. Expected version ">=18". Got "14.21.3" 
// Create the app
var app = express();

// Set up the server
// process.env.PORT is related to deploying on heroku
var server = app.listen(process.env.PORT || 3000, listen);

//Adding some control for rooms and users
var usernames = [];
// rooms which are currently available in chat
var rooms = ['room1','room2','room3']; //NOT USED NOW
var passCodeRooms = ['123','321','456'];//NOT USED NOW


let actualRoom = 0;

// This call back just tells us that the server has started
function listen() {
  var host = server.address().address;
  var port = server.address().port;
  console.log('Example app listening at http://' + host + ':' + port);
}

app.use(express.static('public'));


// WebSocket Portion
// WebSockets work with the HTTP server
var io = require('socket.io')(server);

// Register a callback function to run when we have an individual connection
// This is run for each individual user that connects
io.sockets.on('connection',

  // We are given a websocket object in our function
  function (socket) {

    //const socketId = socket.id;
  
    console.log("We have a new client: " + socket.id);
    usernames.push(socket.id);
    console.log("usernames.length = "+usernames.length);

    //TODO when we get 2 recognized users -> join them into same roomID => "ID1+ID2" ? 
    socket.join(rooms[actualRoom]);//myRoomID
    /*if(usernames.length%2 == 0){
      socket.join(rooms[actualRoom]);//Falta localizar el otro USER y unirlo a la sala
      console.log("joined to "+ rooms[actualRoom]);
      actualRoom++;
    }*/



    //From Desktop Calls
    socket.on('request_new_room',
      function(data) {

        // Data comes in as whatever was sent, including objects
        console.log("Received from Desktop: 'request_new_room' " + data.code + "At socket.id = "+socket.id);
        //TODO push that roomCode and the user ID to join there
        socket.join(data.code);
        console.log("Desktop client created Room and joined");
      }
    );

    socket.on('syncDesktop',
      function(data) {

        // Data comes in as whatever was sent, including objects
        console.log("Received from Desktop: 'syncDesktop' with passcode " + data.code + " From socket.id = "+socket.id);
        console.log("Next Join to this Room and send a message to Desktop");
        //socket.join(data.code)//not necessary to join againg just comunicate is Desktop is sync
        var auxData = {
          code: data.code
        };
        socket.to(data.code).emit('syncDesktop', auxData); //Message from Desktop to Mobile client, no need to send roomID again, Desktop knows

      }
    );


    //From Mobile Calls
    //Last client to join so can say connectio done ( sync )
    socket.on('setupMobile',
      function(data) {

        // Data comes in as whatever was sent, including objects
        console.log("Received from Mobile: 'setupMobile' with passcode " + data.code + " From socket.id = "+socket.id);
        console.log("Next Join to this Room and send a message to Desktop");
        socket.join(data.code)//todo set a real CODE, not the passcode
        var auxData = {
          roomId: data.code
        };
        socket.to(data.code).emit('syncMobile', auxData); //Message from mobile to Desktop client, no need to send roomID again, Desktop knows

        /*
        //TODO read from Data, the CODE, look for this CODE at clients, and send to the right ROOM this message. 
        let auxCode = data.code;
        let auxRoomId = searchCodeUser(auxCode);
        if(auxRoomId != -1){
          console.log("PassCode Found!, Next Join to this Room and send a message");
          socket.join(auxCode)//todo set a real CODE, not the passcode
          var auxData = {
            id: auxRoomId
          };
          socket.to(auxCode).emit('roomCreated', auxData); //Message back to Mobile client
          //socket.to(rooms[auxRoom]).emit('mouse', data); //TODO LETS emit just what I NEED, not mouse
        }
        */

      }
    );

    

    socket.on('emitGetContentData',
      function(data) {
        
        console.log("socket.id= "+socket.id+"..Received : emitGetContentData from idClientType="+ data.idClientType);

        var auxData;

        if(data.idClientType == 1){

          auxData = {
            urlConnectVideo: "assets/CONNECT/Connect-loop-desktop.mp4",
            urlIntroVideo: "assets/INTRO/intro-desktop.mp4",
            urlConnectImage1: "assets/CONNECT/Connect-1-desktop.png", 
            urlConnectImage2: "assets/CONNECT/Connect-2-desktop.png"
          };

        }else if(data.idClientType == 2){ //2 is Mobile

          //assets/CONNECT/Connect-1A-mobile.png", "assets/CONNECT/Connect-1B-mobile.png", "assets/CONNECT/Connect-2-mobile.png", ['assets/INTRO/intro-mobile.mp4', 'assets/INTRO/intro-mobile.webm']);
          auxData = {
            urlConnectImage1A: "assets/CONNECT/Connect-1A-mobile.png",
            urlConnectImage1B: "assets/CONNECT/Connect-1B-mobile.png",
            urlConnectImage2A: "assets/CONNECT/Connect-2-mobile.png",
            urlIntroVideo: "['assets/INTRO/intro-mobile.mp4', 'assets/INTRO/intro-mobile.webm']"
          };
        }

        socket.to(socket.id).emit('emitGetBackContentData', auxData);// Send back to itself client id
        console.log("Emit 'emitGetBackContentData' to socket.id = "+socket.id+" is idClientType = "+ data.idClientType);
        console.log(auxData);
       
      }
    );


    //Receive from Mobile to Desktop instructions to popUpSuggestion image
    socket.on('popUpSuggestion',
      function(data) {
        
        console.log("Received from Mobile: popUpSuggestion at" + data.idQuestion + " for room "+data.code);
        var auxData = {
          code: data.code,
          questionID: data.idQuestion
        };
        socket.to(data.code).emit('popUpSuggestion', auxData);//emit to Desktop..NOT UNDER CONTROL
        //socket.emit('popUpSuggestion', auxData);//TODO emite to Mobile and check if desktop is ready to show POPUPINFO
        console.log("Emit popUpSuggestion with auxData.code = "+auxData.code+" and questionID = "+auxData.questionID);
       
      }
    );


    //Receive from Mobile to Desktop instructions to move page
    socket.on('nextQuestionMobile',
      function(data) {
        
        console.log("Received from Mobile: nextQuestion is" + data.idQuestion + " for room "+data.code);
        var auxData = {
          code: data.code,
          questionID: data.idQuestion
        };
        socket.to(data.code).emit('updateQuestion', auxData);//emit to Desktop..DATA NOT UNDER CONTROL
        socket.emit('updateQuestion', auxData);//emite to Mobile... DATA NOT UNDER CONTROL
        console.log("Emit updateQuestion with auxData.code = "+auxData.code+" and questionID = "+auxData.questionID);
       
      }
    );

  
    socket.on('disconnect', function() {
      usernames.splice(usernames.lastIndexOf(socket.id), 1);
      console.log("Client has disconnected");
    });
  }
);

function searchCodeUser(_auxCode){
  return passCodeRooms.lastIndexOf(_auxCode);

}