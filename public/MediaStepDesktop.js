//Parámeters
//have 1 or 2 vídeos ( first intro then onended second loop )
//have an popup info image for Op2 (the inncorrect option )
//can have an image at loop (intro explication )

//Behauviour
//first loop-desktop on loop 
//May show intro explication if it's available
//wait right response from mobile
//if incorrect -> show popup info image
//if correct -> play _answer-desktop video
//_answer-desktop onendded video -> GO NEXT STATE

let bMediaAudioActivated = false;

function loadNewVideo(bLoop, arrayVideoPaths, videoToLoad){
	if(bDebugDesktop)console.log("------")
	if(bDebugDesktop)console.log("let's loadNewVideo")
	if(bDebugDesktop)console.log(videoToLoad)

	  videoToLoad = createVideo(
      arrayVideoPaths, vidLoad => {
      preloadingItems++;

      //videoToLoad.autoplay(false); //Extra
      //videoToLoad.elt.setAttribute('playsinline', true);//Extra NOT WORKING
      //videoToLoad.elt.setAttribute('autoplay', false);//Extra NOT WORKING
      //videoToLoad.elt.setAttribute('muted', true);//Extra NOT WORKING
	  videoToLoad.volume(0);

      //TODO how to add metadata -> https://stackoverflow.com/questions/26211178/multiple-videos-on-a-single-page-not-loading-all-the-videos-chrome
    });
    videoToLoad.hide();
    


	if(bDebugDesktop)console.log("->")
    //if(bDebugDesktop)console.log(bLoop)
	if(bDebugDesktop)console.log(videoToLoad)

	return videoToLoad;
}

class MediaStepDesktop {

	constructor(_id, _vid0Path, _vid1Path, _popupIntroExplicationImagePath,  _imageConnectPath, _imageStepPath, _imageTIPPath){
		this.id = _id;
		if(bDebugDesktop)console.log("constructor ID ="+this.id)
		if(_popupIntroExplicationImagePath != ""){
			this.popupIntroExplicationImage = loadImage(_popupIntroExplicationImagePath, img => {
					preloadingItems++;
  					});
		}
		if(_imageConnectPath != ""){
			if(bDebugDesktop)console.log("image for Connect available!! path => "+_imageConnectPath)
			this.imageConnect = loadImage(_imageConnectPath, img => {
					preloadingItems++;
  					});
		}
		
		if(_imageStepPath != ""){
			if(bDebugDesktop)console.log("_imageStepPath => "+_imageStepPath)
			this.imageStep = loadImage(_imageStepPath, img => {
					preloadingItems++;
  					});
		}
		
		if(_imageTIPPath != ""){
			if(bDebugDesktop)console.log("_imageTIPPath => "+_imageTIPPath)
			this.imageTIP = loadImage(_imageTIPPath, img => {
					preloadingItems++;
  					});
		}
		
		this.connectTimer = 0;
		this.bNextVideo = false;
		this.letsPlayActualVideo = true;

		this.letsShowPopUpImage = false;
		this.allowShowPopUpImage = true;
		//this.waitingToGoNextQuestion = false;
		this.isFinish = false;

		if(_vid1Path == ""){
			if(bDebugDesktop)console.log("this.vid1 == ''. Let's use just loop video")
			this.bNextVideo = false;// TODO don't go to answer state
			this.isFinish = true;
			this.vid0 = loadNewVideo(true, _vid0Path, this.vid0);//autoplay, videopath, video
		}else{
			if(bDebugDesktop)console.log("*** MediaStepDesktop constructor =>")	
			this.vid0 = loadNewVideo(true, _vid0Path, this.vid0);
			if(bDebugDesktop)console.log("loaded =>"+this.vid0)
			this.vid1 = loadNewVideo(false, _vid1Path, this.vid1);
			if(bDebugDesktop)console.log("loaded =>"+this.vid1)
			if(bDebugDesktop)console.log("***********")	
		}
	}


	finishVid0(){ // If it's called from elt or outside this will differ
		//console.log("Play next from "+ elt.src);
		this.bNextVideo = true;
		this.letsPlayActualVideo = true;
		this.vid0.stop();

		this.connectTimer = millis();
		//console.log("id = "+ this.id +" allowShowPopUpImage "+ this.allowShowPopUpImage);

	}

	finishVid1(elt){
		console.log("Lets GoTo Next Sept StateMachine "+ elt.src);
		//this.waitingToGoNextQuestion = true;
		
		if(this.id != 0){
			console.log("reset to start vars this.id ="+ this.id);
			//reset to start vars
			statusMachine = 0;
			this.connectTimer = 0;
			this.letsPlayActualVideo = true;

		    //allow restart all again if required
		    this.bNextVideo = false;
		    this.letsShowPopUpImage = false;
		}else{ // case init , let's keep waiting with initial loop 
			console.log("Keep looping Init but in waiting mode  ="+ this.id);
			this.bNextVideo = false;
			this.letsPlayActualVideo = true;
		}

	}

	playVid0(){
		//console.log("Play video 0");
		//this.vid0.autoplay(true);
		//this.vid0.elt.muted = true;
		this.vid0.loop();

		//this.vid0.onended(this.finishVid0);
		this.letsPlayActualVideo = false;
	}

	playVid1(){
		//console.log("Play video 1");
		//this.vid0.elt.muted = true;
		this.vid1.play();
		//CHEK NEWWER INFO MORE REALAIABLE
		//if(bSafariVersion )TODO ->
		//SOLUCION poner el audio en el telf y con click activar el sonido.
		//this.vid1.volume(0.7);//not allowed in IOS or webKit if there is not click -> https://webkit.org/blog/6784/new-video-policies-for-ios/
		this.vid1.onended(this.finishVid1.bind(this));//usind bind for call this ( as class and not as media elt ) inside finishVid1 
		this.letsPlayActualVideo = false;
	}

	displayAnswer(){
		imageMode(CENTER);
		if(this.id == 0 && millis() - this.connectTimer < 1000){//case Connect + Intro
			//console.log("Display this.id == 0 while 1 second");
			image(this.vid0, 0.5*w, 0.5*h, w, this.vid0.h*w/this.vid0.width);
			image(this.imageConnect, 0.5*w, 0.5*h, w, this.imageConnect.height*w/this.imageConnect.width);
		}else{
			if(this.letsPlayActualVideo)this.playVid1();
			//console.log("Display video 1");
			image(this.vid1, 0.5*w, 0.5*h, w, this.vid1.height*w/this.vid1.width);
		}
	}

	display(){//Display loop ( vid0 ) or answer ( vid1) -> bNextVideo decides


		imageMode(CENTER);

		if(this.bNextVideo == false){ // Vid0 -> loop
			
			if(this.letsPlayActualVideo)this.playVid0();
			
			image(this.vid0, 0.5*w, 0.5*h, w, this.vid0.height*w/this.vid0.width);

		}else { // Vid1 -> answer
			//Remove all extra info
			this.displayAnswer();
		}


	}
}

