let input2, button2;
let roomIdMobile = -1;
let bSyncMobile = false;
let bDebugMobile = false;

//mobile
function greet2() {

	//Hack auto pass sync code access 
	const auxData = 12345;//input2.value();

	var data = {
	code: auxData,
	time: millis()
	};

	if(bDebugMobile)console.log("emit setupMobile");
	if(bDebugMobile)console.log(socket);
	if(bDebugMobile)console.log(data);

	socket.emit('setupMobile',data);

/*
	//greeting2.html('Trying to connect with passCode ' + auxData);
	//greeting2.hide();
	input2.hide();
	//button2.hide();

	//hack to mobile start or cleant at change status
	*/
}


function setupInput2HTML(){

 /*
  let deltaY = 0;
  let colorBkUI = color(255);
  
  input2 = createInput('');
 input2.position(w*0.15, h*.455);
  input2.size(w*0.3, h*0.05);
  input2.style('font-size', '40px');
  input2.style('background-color', colorBkUI);

  //greeting2 = createElement('h2', '(Mobile) Code to Connect');
  //greeting2.position(20, 5+deltaY);


  //Start All Hide Until Assets loaded
  input2.hide();
  //button2.hide();
  //greeting2.hide();
*/
  textAlign(CENTER);
  textSize(50);
}



//Mobile Client Setup Socket
function setupSocketsMobileClient(){
  // Start a socket connection to the server
  //socket = io.connect(URLNODEJS);//TRY to setup this before login

  if(bDebugMobile)console.log("socket io.connect");
  if(bDebugMobile)console.log(socket)

  socket.on('emitGetBackContentData',

    // When we receive data
    function(data) {
    	console.log("From mobile Got: emitGetBackContentData");
      if(bDebugDesktop){
      	console.log("Got: emitGetBackContentDataDesktop data=");
      	console.log(data);
      }

    }
  );

  //Simple way
  socket.on("syncDesktop", function (auxData) {
  	//if(!bSyncMobile){//Not necessary trying to check if several restarts available
	  	if(true)console.log("Desktop Sync , so set Mobile 'sync' too");
	    if(bDebugMobile)console.log("room roomIdDesktop from server is: " + auxData.code);
	    roomIdMobile = auxData.code;
	    
	    //statusMachine = 1;//mobile start at 1 for the question 1 also
	    statusMachine = 0;
	    if(mediaMobileStates[statusMachine].isFinish == false){
	     	mediaMobileStates[statusMachine].finishImg0();
	    }

	    bSyncMobile = true;
  });

  socket.on("updateQuestion", function (auxData) {
  	if(bSyncMobile){
	  	if(bDebugMobile)console.log("let's updateQuestion at Mobile");
	    if(bDebugMobile)console.log("Next questions is: " + auxData.questionID);
	    //roomIdDesktop = auxData.id;

	    //trying fast assingment test
	    statusMachine = auxData.questionID;
	    
	    if(mediaMobileStates[statusMachine].isFinish == false){
	     	mediaMobileStates[statusMachine].finishImg0();
	    }else{
	    	if(bDebugDesktop)console.log("There is no answer question so We might be at then end.")
	    }

  	}

  });

/*
   socket.on("popUpSuggestion", function (auxData) {
  	if(bSyncMobile){
	  	if(bDebugMobile)console.log("let's popUpSuggestion at Mobile");
	    if(bDebugMobile)console.log("Next questions is: " + auxData.questionID);
	    //roomIdDesktop = auxData.id;
	    //mediaDesktopStates[statusMachine].letsShowPopUpImage = true;
  	}

  });
  */
  
}


/*
///---------NEXT USE CLASSES 

class ClientMobile {
	constructor(){
		this.socketID = "";
	}

	connectedAtServer(_sockectID){
		this.socketID = _sockectID;
	}

	getSockedId(){
		return this.socketID;
	}

	requestPair2Room(_roomId){
		data = {
			id: this.socketID,
			code: _roomId
		};

		socket.emit('requestPair',data)
	}
}
*/