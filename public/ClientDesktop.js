let inputSecure, buttonSecure, greetingSecure;
let input1, button1, greeting1;
let roomIdDesktop = -1;
let bSyncDesktop = false;
let bDebugDesktop = true;

//desktop

//secure options
let bAllowedEntry = false;

function setupInputSecureHTML(){

let posYInitText = 20;
let posYInput = posYInitText*6;
let posX = 60;

let sizeWInput = 100;
let fontSize = 40;

  greetingSecure = createElement('h2', 'Please enter password');
  greetingSecure.style('font-size', '40px');
  greetingSecure.style('text-align', 'center');
  greetingSecure.position(posX, posYInitText);
  
  //greetingSecure.style('background-color', colorBkUI);
  greetingSecure.style('font-family', 'Helvetica');
  greetingSecure.style('font-weight', 'bold');


  inputSecure = createInput('','password');
  inputSecure.style('font-size', fontSize+'px');
  inputSecure.position(posX, posYInput);
  inputSecure.size(sizeWInput, fontSize);
  inputSecure.style('font-family', 'Helvetica');
  //inputSecure.style('font-size', '40px');
  //inputSecure.style('background-color', colorBkUI);
  //inputSecure.style('font-family', 'Helvetica');
  //inputSecure.style('font-weight', 'bold');
  //inputSecure.hide();

  buttonSecure = createButton('Submit');
  buttonSecure.style('font-family', 'Helvetica');
  buttonSecure.style('font-size', '20px');
  buttonSecure.size(sizeWInput, fontSize+6);
  buttonSecure.position(posX+sizeWInput+10, posYInput);
  buttonSecure.mousePressed(greetSecure);



}

function greetSecure() {

	const myCodeSecure = "now";//inputSecure.value();
	//check if code exist myCodeSecure in array of passwords
	if (myCodeSecure == "now" || myCodeSecure == "Now" || myCodeSecure == "NOW") {
	  /*inputSecure.hide();
	  buttonSecure.hide();
	  greetingSecure.hide();*/
	  //Start Desktop mode
	  setupSocketsDesktopClient();

	  ////Hack auto pass sync code access 
	  randServerPassCode = 12345;//random(0, 999);
	  //randServerPassCode = floor(nf(randServerPassCode, 3, 0));
	  requestRoomServerDesktop(randServerPassCode);
	  statusMachine = 0;
	}

}



//Function disabled in order to create random password
function setupInput1HTML(){
  input1 = createInput();
  input1.position(20, 65);

  button1 = createButton('submit');
  button1.position(150, 65);
  button1.mousePressed(greet1);

  greeting1 = createElement('h2', '(Desktop)Code to share');
  greeting1.position(20, 5);

  textAlign(CENTER);
  textSize(50);
}
//Function disabled in order to create random password
function greet1() {

  const myRoomID = input1.value();

  greeting1.html('Connect your playbook. CODE: ' + myRoomID);
  requestRoomServerDesktop(myRoomID);
  //roomIdDesktop = myRoomID; //provisional mode room id save, ideal will be on server side.
  input1.hide();
  button1.hide();

}

//////////////////////////////
//request to server
function requestRoomServerDesktop(_codeRoom) {
  let data = {
    id: this.socketID, //redundant info
    code: _codeRoom
  };

  socket.emit('request_new_room', data)

  roomIdDesktop = _codeRoom; //myRoomID; //provisional mode room id save, ideal will be on server side.
}


//Desktop Client Setup Socket
function setupSocketsDesktopClient(){

  ///socket = io.connect(URLNODEJS); //TRY to setup this before login

  //to handle responses questions from Mobile Client
  socket.on('pressedButton',
    // When we receive data
    function(data) {
      if(bDebugDesktop)console.log("Got: " + data.buttonId + " " + data.statusId);
      // Draw a blue circle
      fill(255,0,255);
      stroke(0);
      text("Button Pressed id["+data.buttonId+"] question["+data.statusId+"]", random(100, w-100), random(100, h-100));
    }
  );

  socket.on('connected',
    // When we receive data
    function(data) {
      if(bDebugDesktop)console.log("Got: " + data.sockedID);
      //connectedAtServer(data.sockedID);//TODO with Class
    }
  );

//TODO RESEARCH WHY THIS IS NOT BEING USED, CALLED OR RECEIVED
  socket.on('emitGetBackContentData',
  	function(data) {
    	console.log("From desktop Got: emitGetBackContentData");
      if(bDebugDesktop){
      	console.log("Got: emitGetBackContentDataDesktop data=");
      	console.log(data);
      }

    }
  );
 

  //Simple way
  socket.on("syncMobile", function (auxData) {
  	//if(!bSyncDesktop){ //this may open the oportunitu to another participant to get it... Dangerous or not?
	    //console.log("room roomIdDesktop from server is: " + auxData.id);
	    //roomIdDesktop = auxData.id;
	    bSyncDesktop = true;

	    statusMachine = 0; //!!!!**** Here we Start the Experience !!!!****
	    mediaDesktopStates[statusMachine].finishVid0(); //fast way to go next internal step video.. Should improve this into something more universal and clear..

	  	if(true)console.log("Desktop Socket.on 'sync'. Now confirm to Mobile sync");
		var auxData = {
			code: roomIdDesktop,
			time: millis()
		};

	    socket.emit('syncDesktop', auxData)
	    if(true)console.log("emit syncDesktop");

  	//}

  });

  socket.on("updateQuestion", function (auxData) {
  	if(bSyncDesktop){
	  	if(bDebugDesktop)console.log("let's updateQuestion at Desktop");
	    if(bDebugDesktop)console.log("Next questions is: " + auxData.questionID);
	    //roomIdDesktop = auxData.id;
	    
	    //trying fast assingment test
	    statusMachine = auxData.questionID;

	    if(mediaDesktopStates[statusMachine].isFinish == false){ // we keep in vid0 ( video looping) at the end
	     	mediaDesktopStates[statusMachine].finishVid0(); //fast way to go next internal step video.. Should improve this into something more universal and clear..
	    }else{
	    	if(bDebugDesktop)console.log("There is no answer question so We might be at then end.")

	    }

	    //updatePlayVideoStatus();
  	}

  });



}
