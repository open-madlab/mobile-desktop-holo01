//by Carles Gutiérrez
//2022

let bIOS = false;

let w, h;
let wPosX, hPosY;
let marginX = 90;//TODO detect the right margin for each IOS

var socket;
let bSocketSetup = false;

let URLNODEJS = "https://mobile-desktop-holo01.onrender.com";
//let URLNODEJS = "http://localhost:3000";
let clientType = -1;//default client type is Desktop. 

let clientDesktop;//1 //CLASS NOT USED TODO REMOVE
let clientMobile;//2 //CLASS NOT USED TODO REMOVE

//Maquina de estados
let statusMachine = -1;//0 ( waiting or conected), 1(question 1), 2(question 2), ... 
let preloadingItems = 0;

//New Desktop class to use at each state
//Parámeters
//have 1 or 2 vídeos ( first intro then onended second loop )
//have an popup info image for Op2 (the inncorrect option )
//can have an image at loop (intro explication )
//Behauviour
//first loop-desktop on loop 
//May show intro explication if it's available
//wait right response from mobile
//if incorrect -> show popup info image
//if correct -> play _answer-desktop video
//_answer-desktop onendded video -> GO NEXT STATE
let mediaDesktopStates = [];
let mediaMobileStates = [];


//interaction vars
//let totalQuestions = 6;//Then last one is Completed
let nextActionPressed = false;
let lastMousePressedCounter =0;
let lastInteractionTime = 0; 

//loading options
let angle = 0;
let deltaAngle = 0.1;//Desktop And // '0.2' for mobile?
let bLoading = true;

function loadAllAssets(){
  //Load all images

  if(clientType == 1){
                                              //_id, _vid0Path, _vid1Path, _popupIntroExplicationImagePath
    mediaDesktopStates[0] = new MediaStepDesktop(0, ["assets/CONNECT/Connect-loop-desktop.mp4"], ["assets/INTRO/intro-desktop.mp4"], "assets/CONNECT/Connect-1-desktop.png", "assets/CONNECT/Connect-2-desktop.png", "", "");
    mediaDesktopStates[1] = new MediaStepDesktop(1, [""], ["assets/STEP_1/STEP1_answer-desktop.mp4"], "", "", "assets/STEP_1/STEP1_stepnumber-desktop.png", "", "");
    mediaDesktopStates[2] = new MediaStepDesktop(2, [""], ["assets/STEP_2/STEP2_answer-desktop.mp4"], "", "", "assets/STEP_2/STEP2_stepnumber-desktop.png", "");
    mediaDesktopStates[3] = new MediaStepDesktop(3, [""], ["assets/STEP_3/STEP3_answer-desktop.mp4"], "", "", "assets/STEP_3/STEP3_stepnumber-desktop.png", "");
    mediaDesktopStates[4] = new MediaStepDesktop(4, [""], ["assets/STEP_4/STEP4_answer-desktop.mp4"], "", "", "assets/STEP_4/STEP4_stepnumber-desktop.png", "");
    mediaDesktopStates[5] = new MediaStepDesktop(5, [""], ["assets/STEP_5/STEP5_answer-desktop.mp4"], "", "", "assets/STEP_5/STEP5_stepnumber-desktop.png", "");
    mediaDesktopStates[6] = new MediaStepDesktop(6, [""], ["assets/STEP_6/STEP6_answer-desktop.mp4"], "", "", "assets/STEP_6/STEP6_stepnumber-desktop.png", "");
    mediaDesktopStates[7] = new MediaStepDesktop(7, ["assets/COMPLETED/Completed-desktop.mp4"], "", "", "", "", "");

  }
  else if(clientType == 2){


    mobileConnected = loadImage('assets/CONNECT/Connect-2-mobile.png', img => {
      preloadingItems++;
    });

/*
    mediaMobileStates[0] = new MediaStepMobile(0, "assets/CONNECT/Connect-1A-mobile.png", "assets/CONNECT/Connect-1B-mobile.png", "assets/CONNECT/Connect-2-mobile.png", ['assets/INTRO/intro-mobile.mp4', 'assets/INTRO/intro-mobile.webm']);
    mediaMobileStates[1] = new MediaStepMobile(1, "", "", "", ['assets/STEP_1/STEP1_answer-mobile.mp4', 'assets/STEP_1/STEP1_answer-mobile.webm']);
    mediaMobileStates[2] = new MediaStepMobile(2, "", "", "", ['assets/STEP_2/STEP2_answer-mobile.mp4', 'assets/STEP_2/STEP2_answer-mobile.webm']);
    mediaMobileStates[3] = new MediaStepMobile(3, "", "", "", ['assets/STEP_3/STEP3_answer-mobile.mp4', 'assets/STEP_3/STEP3_answer-mobile.webm']);
    mediaMobileStates[4] = new MediaStepMobile(4, "", "", "", ['assets/STEP_4/STEP4_answer-mobile.mp4', 'assets/STEP_4/STEP4_answer-mobile.webm']);
    mediaMobileStates[5] = new MediaStepMobile(5, "", "", "", ['assets/STEP_5/STEP5_answer-mobile.mp4', 'assets/STEP_5/STEP5_answer-mobile.webm']);
    mediaMobileStates[6] = new MediaStepMobile(6, "", "", "", ['assets/STEP_6/STEP6_answer-mobile.mp4', 'assets/STEP_6/STEP6_answer-mobile.webm']);
    mediaMobileStates[7] = new MediaStepMobile(7, "assets/COMPLETED/Completed-mobile.png", "", "", "");
*/

    mediaMobileStates[0] = new MediaStepMobile(0, "assets/CONNECT/Connect-1A-mobile.png", "assets/CONNECT/Connect-1B-mobile.png", "assets/CONNECT/Connect-2-mobile.png", ['assets/INTRO/intro-mobile.mp4', 'assets/INTRO/intro-mobile.webm']);
    mediaMobileStates[1] = new MediaStepMobile(1, "assets/STEP_1/STEP1_1-mobile.png", "assets/STEP_1/STEP1_2-mobile.png", "assets/STEP_1/STEP1_4-mobile.png", ['assets/STEP_1/STEP1_answer-mobile.mp4', 'assets/STEP_1/STEP1_answer-mobile.webm']);
    mediaMobileStates[2] = new MediaStepMobile(2, "assets/STEP_2/STEP2_1-mobile.png", "assets/STEP_2/STEP2_2-mobile.png", "assets/STEP_2/STEP2_4-mobile.png", ['assets/STEP_2/STEP2_answer-mobile.mp4', 'assets/STEP_2/STEP2_answer-mobile.webm']);
    mediaMobileStates[3] = new MediaStepMobile(3, "assets/STEP_3/STEP3_1-mobile.png", "assets/STEP_3/STEP3_2-mobile.png", "assets/STEP_3/STEP2_4-mobile.png", ['assets/STEP_3/STEP3_answer-mobile.mp4', 'assets/STEP_3/STEP3_answer-mobile.webm']);
    mediaMobileStates[4] = new MediaStepMobile(4, "assets/STEP_4/STEP4_1-mobile.png", "assets/STEP_4/STEP4_2-mobile.png", "assets/STEP_4/STEP2_4-mobile.png", ['assets/STEP_4/STEP4_answer-mobile.mp4', 'assets/STEP_4/STEP4_answer-mobile.webm']);
    mediaMobileStates[5] = new MediaStepMobile(5, "assets/STEP_5/STEP5_1-mobile.png", "assets/STEP_5/STEP5_2-mobile.png", "assets/STEP_5/STEP2_4-mobile.png", ['assets/STEP_5/STEP5_answer-mobile.mp4', 'assets/STEP_5/STEP5_answer-mobile.webm']);
    mediaMobileStates[6] = new MediaStepMobile(6, "assets/STEP_6/STEP6_1-mobile.png", "assets/STEP_6/STEP6_2-mobile.png", "assets/STEP_6/STEP2_4-mobile.png", ['assets/STEP_6/STEP6_answer-mobile.mp4', 'assets/STEP_6/STEP6_answer-mobile.webm']);
    mediaMobileStates[7] = new MediaStepMobile(7, "assets/COMPLETED/Completed-mobile.png", "", "", "");

  
  }
}

function preLoadedActions(){

  if(bDebugDesktop)console.log("Manual preLoadedActions. If you add new elements modify here the number for desktop and mobile")

  if( (bLoading == true && clientType == 2 && preloadingItems >= 17 /*24*/) || //mobile
      (bLoading == true && clientType == 1 && preloadingItems >= 7) //desktop
    ){
      //Finish Loading
      bLoading = false;
      if(bDebugMobile)console.log("Loaded ALL Items = "+ preloadingItems);

    //Quick action before to start 
    if (clientType == 1) {

    } else if (clientType == 2) {
      if(bDebugDesktop)console.log("preLoadedActions");
      //input2.show();
      //button2.show();
    }

    //Hack sync
    startCodeAccessDesckAndMobile();// TODO move later

  }
}

function setupSockets(){
  socket = io.connect(URLNODEJS);

  if (clientType == 1) {
    //setupInputSecureHTML();//Removed pass

  } else if (clientType == 2) {
    setupSocketsMobileClient();
    setupInput2HTML();//Removed pass

  }
}


function canvasSetupAndResize(_sketch, bResize){

  if(bResize)resizeCanvas(windowWidth, windowHeight);//windowWidth, windowHeight //1920, 1080 //640, 480 //TODO ISSUE IOS?
  else {
    _sketch = createCanvas(windowWidth, windowHeight);
    _sketch.position(0, 0);
    _sketch.style("z-index", "-1");
  }

  console.log("Dims w="+ width + " h="+ height);
  console.log("Dims windowWidth="+ windowWidth + " windowHeight ="+ windowHeight);

  w = windowWidth;//TODO for resizing and factorScreen options
  h = windowHeight;
}

function windowResized() {
  let auxsketch;//not used
  canvasSetupAndResize(auxsketch, true); //TODO fix sketch not at setup or draw
}

function preload(){

  if(mobileAndTabletCheck()){
    clientType = 2; //console.log("bMobileVersion");
    deltaAngle = 0.2;
  }else {
    clientType = 1;
  }

}

function setup() {

  let sketch;

  bIOS = detectIOS();
  //pixelDensity(1);
  canvasSetupAndResize(sketch, false);
  //mimics the autoplay policy TODO
  //getAudioContext().suspend();//GOOD IDEA BUT NOT WORKING



  //
  setupSockets();

  console.log("Preload Done");

  //Load all data
  loadAllAssets();


  emitGetContentData();//TODO replay at server with URL media


}

function drawConnection(){
    push();
  
  if (clientType == 1) {
    if(bSyncDesktop){
      fill(0, 255, 0)
    }else {
      fill(255, 0, 0)
    }

    noStroke();
    ellipse(10, 10, 20, 20);

  } else if (clientType == 2) {
   
    if(bSyncMobile){
      fill(0, 255, 0)
    }else {
      fill(255, 0, 0)
    }

    noStroke();
    ellipse(10, 10, 20, 20);
  }

  pop();
}


function drawStatusMachine(){
  //updateStatusMachine();

  //Draw questions
  if(statusMachine >-1){
    if(clientType == 1)mediaDesktopStates[statusMachine].display();
    else if(clientType == 2)mediaMobileStates[statusMachine].display();    
  }else if(statusMachine == -1){
    if(clientType == 2)mediaMobileStates[0].display();
  }

}

function calImgResize(_image){
  let resPosSize = [];

  if(!bIOS){ //Fullscreen mode
    resPosSize[0] = w*0.5;
    resPosSize[1] = h*0.5;
    resPosSize[2] = w;
    resPosSize[3] = (_image.height*w/_image.width);
  }
  else{ //IOS Mode with regular manual margin //TODO improve size margin auto
    resPosSize[0] = w*0.5;
    resPosSize[1] = h*0.5;
    resPosSize[2] = w-marginX/2;
    resPosSize[3] = (_image.height*w/_image.width)-marginX;
  }


  return resPosSize;
}

function draw() {
  
  if(bIOS){
    background(0);    
  }else background(255);

  //background("#DFDFDF");

  if(bLoading){
    
    push();
    translate(w/2, h/2);
    rotate(angle);
    //strokeWeight(4);
    //stroke(0,0,255);
    noStroke();
    fill(0,0,255);
    ellipse(100,0, 20, 20);
    angle += deltaAngle;
    pop();

    //check preloading 
    preLoadedActions();

  }
  else{ 
    drawConnection();
    drawStatusMachine();
  }

  

}

/* TODO TRYING HEREEEEEEEE TO GET INITIAL CONTENT FROM AT PRELOADING::::..... might not work if its at setup but... */
function emitGetContentData(){
      
    //lets popup image
    console.log("emitGetContentData with clientType = " +clientType);

    var data = {
      idClientType: clientType,
      time: millis()
    };

    if(bDebugDesktop)console.log(" ^^^^^^^^^^^^^^^^^^^^ ^^^^^ emit GetContentData to server  ^^^^^^^^^^^^^^^^ ^^^^^^^^^^^^");

    socket.emit('emitGetContentData',data);
}

function emitPopUpSuggestion(){
      //lets popup image
      if(bDebugMobile)console.log("lets popup image at "+statusMachine);
      if(bDebugMobile)console.log("roomIdMobile =" +roomIdMobile);

      var data = {
        code: roomIdMobile,
        idQuestion: statusMachine,
        time: millis()
      };

      if(bDebugMobile)console.log("emit popUpSuggestion");

      socket.emit('popUpSuggestion',data);
}

function emitNextQuestion(){
    if(bDebugMobile)console.log("nextQuestion = "+statusMachine);  
    if(bDebugMobile)console.log("roomIdMobile =" +roomIdMobile);

    var data = {
      code: roomIdMobile,
      idQuestion: statusMachine,
      time: millis()
    };

    if(bDebugMobile)console.log("emit nextQuestionMobile");

    socket.emit('nextQuestionMobile',data);
}

//Hack on keyPressed
function keyPressed() {

    if (key === '1') {
      statusMachine = 1;
      emitNextQuestion();
      //mediaDesktopStates[1].bNextVideo = true;
    } else if (key === '2') {
      statusMachine = 2;
      emitNextQuestion();
      //mediaDesktopStates[2].bNextVideo = true;
    } else if (key === '3') {
      statusMachine = 3;
      emitNextQuestion();
      //mediaDesktopStates[3].bNextVideo = true;
    } else if (key === '4') {
      statusMachine = 4;
      emitNextQuestion();
      //mediaDesktopStates[4].bNextVideo = true;
    } else if (key === '5') {
      statusMachine = 5;
      emitNextQuestion();
      //mediaDesktopStates[5].bNextVideo = true;
    } else if (key === '6') {
      statusMachine = 6;
      emitNextQuestion();
      //mediaDesktopStates[6].bNextVideo = true;
    }

    

}

 //AUTOMATE sync code access , LETS ADD IMAGE BUTTON START
//hack sync code access 
function startCodeAccessDesckAndMobile(){

  //Desktop
  if(statusMachine == -1 && clientType == 1){
    greetSecure();

    //a way to activate volume after interaction that helps to pass webkit policies
    for(let i = 0; i < mediaDesktopStates.length-1; i++){
      if(bDebugMobile)console.log("mediaDesktopStates.length = "+mediaDesktopStates.length);
      if(bDebugMobile)console.log("keyPressed RETURN -> mediaDesktopStates[i].vid1.volume(1); i="+i);
      mediaDesktopStates[i].vid1.volume(1); //this Idea came from setInterval ( after interactio add permisions ) -> https://developer.mozilla.org/en-US/docs/Web/Media/Autoplay_guide
    }
  }

  //Mobile
  if(statusMachine == 0 && clientType == 2){
    greet2(); //Should be called after START BUTTON //TODO add start button
  }
}

function mousePressed(){
  //userStartAudio(); //TODO
  //getAudioContext().resume();//Not workkin ios
  //console.log("getAudioContext().resume();");//ok but now working
}

// this function fires with any touch anywhere
function touchStarted() {
  if(clientType == 2 && !bMediaAudioActivated){

    //a way to activate volume after interaction that helps to pass webkit policies
    if(bDebugMobile)console.log("mediaMobileStates.length = "+mediaMobileStates.length);
    for(let i = 0; i < mediaMobileStates.length-1; i++){
      
      if(bDebugMobile)console.log("mouseReleased -> mediaMobileStates[i].vid1.volume(1); i="+i);
      if(bDebugMobile)console.log("elt.src = "+mediaMobileStates[i].vid1.src);

      //issues policies IOS p5js ( muted + playsine + autoplay ) // https://github.com/processing/p5.js/issues/1846
      mediaMobileStates[i].vid1.volume(0); //Required to avoid IOS policies -> https://developer.mozilla.org/en-US/docs/Web/Media/Autoplay_guide
      mediaMobileStates[i].vid1.autoplay(false); //Required to avoid IOS policies
      mediaMobileStates[i].vid1.elt.setAttribute('playsinline', true);//Required to avoid IOS policies
    }

    bMediaAudioActivated = true;
  }
}

function mouseReleased() {

 //////////////////////////////////////////
 //Media activation
  //FOR DESKTOP INTERACTION
  if(clientType == 1 && !bMediaAudioActivated){

    //a way to activate volume after interaction that helps to pass webkit policies
    if(bDebugMobile)console.log("mediaDesktopStates.length = "+mediaDesktopStates.length);
    for(let i = 0; i < mediaDesktopStates.length-1; i++){
      if(bDebugMobile)console.log("mouseReleased -> mediaDesktopStates[i].vid1.volume(1); i="+i);
      if(bDebugMobile)console.log("src = "+mediaDesktopStates[i].vid1.src);
      mediaDesktopStates[i].vid1.volume(1); //this Idea came from setInterval ( after interactio add permisions ) -> https://developer.mozilla.org/en-US/docs/Web/Media/Autoplay_guide
    }
    bMediaAudioActivated = true;

  }

  //////////////////////////////////////////

  //FOR MOBILE INTERACTION
  if(clientType == 2 && !bSyncMobile && (mouseY > h*0.75)){
    greet2();

    if(!bIOS){
      fullscreen(true);
      console.log("fullscreen mode active");
      console.log("Dims w="+ width + " h="+ height);
      console.log("Dims windowWidth="+ windowWidth + " windowHeight ="+ windowHeight);      
    }
    
  }
  else if(clientType == 2 && bSyncMobile && millis()-lastInteractionTime > 1000){

    //Hack for las question (please TODO better !!!! )
    if(statusMachine == mediaMobileStates.length-1){//last screen , let's Restart
        greet2(); //lets simply start again
        console.log("Restart from mobile");
    }
    else if(statusMachine == mediaMobileStates.length-2){
        emitNextQuestion();
    }
    else if(statusMachine < mediaMobileStates.length-2){//totalQuestions

      let bPressedUP = (mouseY < h*0.5);
      //console.log("bPressedUP = "+bPressedUP);
      
      //nextActionPressed = !nextActionPressed; // true ( lets popup image) , false ( next Question) 
      if(!bPressedUP){
        emitPopUpSuggestion();
        mediaMobileStates[statusMachine].bPopUpInfoPressed = true;
      }
      else{
        //go to nextQuestion
        emitNextQuestion();
        mediaMobileStates[statusMachine].bUpOptionPressed = true;
      }
    }
    else{
      if(bDebugMobile)console.log("No more StatesMachine available");
    }
     


      //avoid issues interaction double event
      lastInteractionTime = millis();
      
  } 
}

////////////////////////////
//MOBILE
window.mobileAndTabletCheck = function () {
  let check = false;
  (function (a) {
    if (
      /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(
        a
      ) ||
      /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
        a.substr(0, 4)
      )
    )
      check = true;
  })(navigator.userAgent || navigator.vendor || window.opera);
  return check;
};

////DETECT IF IOS
function detectIOS(){
  var isIOS = /iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  return isIOS;
}
