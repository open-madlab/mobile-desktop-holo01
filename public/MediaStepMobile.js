// draw an arrow for a vector at a given base position
// from p5js reference

function drawArrow(base, vec, myColor) {
  push();
  stroke(myColor);
  strokeWeight(7);
  fill(myColor);
  translate(base.x, base.y);
  line(0, 0, vec.x, vec.y);
  rotate(vec.heading());
  let arrowSize = 23;
  translate(vec.mag() - arrowSize, 0);
  triangle(0, arrowSize / 2, 0, -arrowSize / 2, arrowSize, 0);
  pop();
}

class MediaStepMobile {

	constructor(_id, _img0ButtonPath, _img0ButtonOp1DonePath, _img0ButtonOp2DonePath,_vid1Path){
		this.id = _id;
		//console.log("constructor mobile ID ="+this.id)
		if(_img0ButtonPath != ""){
			preloadingItems++;
			this.img0ButtonPath = loadImage(_img0ButtonPath, img => {
					preloadingItems++;
  					});
		}

		this.connectTimer = 0;
		this.bNextVideo = false;
		this.letsPlayActualVideo = false;//true;
		this.isFinish = false;
		this.bPopUpInfoPressed = false;
		this.bUpOptionPressed = false;

		this.v0 = createVector(w*0.7, h*0.43, 0);
		this.v1 = createVector(w*0.2, 0);

		if(_vid1Path == ""){
			if(bDebugDesktop)console.log("this.vid1 == ''. Let's use just loop video")
			this.bNextVideo = false;// TODO don't go to answer state
			this.isFinish = true;
		}else{
			if(bDebugDesktop)console.log("*** MediaStepMobile constructor =>")	
			this.vid1 = loadNewVideo(false, _vid1Path, this.vid1);
			if(bDebugDesktop)console.log("loaded =>"+this.vid1)
			if(bDebugDesktop)console.log("***********")	
		}


	}


	finishImg0(){ // If it's called from elt or outside this will differ
		this.bNextVideo = true;
		this.letsPlayActualVideo = true;
		this.connectTimer = millis();
	}

	//to allow reset and restart questions
	resetStatusValues(){
		this.bNextVideo = false;
		this.bPopUpInfoPressed = false;
		this.bUpOptionPressed = false;
	}

	finishVid1(elt){
		//console.log("MediaStepMobile -> Lets GoTo Next Sept StateMachine  "+ elt.src);
		this.resetStatusValues();
		//statusMachine++;
		//statusMachine = 0;
	  //  if(statusMachine >mediaMobileStates.length-1)statusMachine =mediaMobileStates.length-1;
	}

	playVid1(){
		//console.log("MediaStepMobile -> Play video 1");
		let startPlayPromise = this.vid1.play();

		this.vid1.onended(this.finishVid1.bind(this));//usind bind for call this ( as class and not as media elt ) inside finishVid1 
		this.letsPlayActualVideo = false;
		this.bUpOptionPressed = true;
		
	}

	displayAnswer(){
		imageMode(CENTER);
		if(this.id == 0 && millis() - this.connectTimer < 1000){//case Connect + Intro
			//console.log("MediaStepMobile Display this.id == 0 while 1 second");
			let auxRes = calImgResize(mobileConnected);
			image(mobileConnected, auxRes[0], auxRes[1], auxRes[2], auxRes[3]);
		}else{
			if(this.letsPlayActualVideo)this.playVid1();
			//console.log("Display video 1");
			let auxRes = calImgResize(mobileConnected);//works better use same dimension
			image(this.vid1, auxRes[0], auxRes[1], auxRes[2], auxRes[3]);	
		}
	}

	display(){//Display loop ( seleccion images ) or answer ( vid1) -> bNextVideo decides
		imageMode(CENTER);
		if(this.bNextVideo == false){ // State 0 -> show buttons images 
			let auxRes = calImgResize(this.img0ButtonPath);
			image(this.img0ButtonPath, auxRes[0], auxRes[1], auxRes[2], auxRes[3]);
			if(this.bPopUpInfoPressed){

				//Draw pressed button effect //TODO
				push();
				fill(100, 100);
				rect(0, h*.5, w, h*.5);
				pop();

  				drawArrow(this.v0, this.v1, 'white');
			}

			if(this.bUpOptionPressed){
				
				//Draw pressed button effect
				push();
				fill(100, 100);
				rect(0, 0, w, h*.5);
				pop();
			}
		}else { // Vid1 -> answer
			this.displayAnswer();
		}

	}
}